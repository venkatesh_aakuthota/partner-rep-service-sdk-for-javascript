import PartnerRepServiceSdkConfig from './partnerRepServiceSdkConfig';
import DiContainer from './diContainer';
import AddPartnerRepReq from './addPartnerRepReq';
import AddPartnerRepFeature from './addPartnerRepFeature';
import GetPartnerRepBankInfoUpdateUrlReq from './getPartnerRepBankInfoUpdateUrlReq';
import GetPartnerRepBankInfoUpdateUrlFeature from './getPartnerRepBankInfoUpdateUrlFeature';
import GetPartnerRepW9UpdateUrlReq from './getPartnerRepW9UpdateUrlReq';
import GetPartnerRepW9UpdateUrlFeature from './getPartnerRepW9UpdateUrlFeature';
import GetPartnerRepWithIdFeature from './getPartnerRepWithIdFeature';
import ListPartnerRepsWithAccountIdFeature from './listPartnerRepsWithAccountIdFeature';
import PartnerRepSynopsisView from './partnerRepSynopsisView';
import PartnerRepView from './partnerRepView';
import SearchForPartnerRepWithEmailAddressFeature from './searchForPartnerRepWithEmailAddressFeature';
import UpdatePartnerRepFirstNameFeature from './updatePartnerRepFirstNameFeature';
import UpdatePartnerRepLastNameFeature from './updatePartnerRepLastNameFeature';
import UpdatePartnerRepPhoneNumberFeature from './updatePartnerRepPhoneNumberFeature';
import UpdatePartnerRepPostalAddressFeature from './updatePartnerRepPostalAddressFeature';
import UpdatePartnerRepFirstNameReq from './updatePartnerRepFirstNameReq';
import UpdatePartnerRepLastNameReq from './updatePartnerRepLastNameReq';
import UpdatePartnerRepPhoneNumberReq from './updatePartnerRepPhoneNumberReq';
import UpdatePartnerRepPostalAddressReq from './updatePartnerRepPostalAddressReq';

/**
 * @class {PartnerRepServiceSdk}
 */
export default class PartnerRepServiceSdk {

    _diContainer:DiContainer;

    /**
     * @param {PartnerRepServiceSdkConfig} config
     */
    constructor(config:PartnerRepServiceSdkConfig) {

        this._diContainer = new DiContainer(config);

    }

    addPartnerRep(request:AddPartnerRepReq,
                  accessToken:string):Promise<string> {

        return this
            ._diContainer
            .get(AddPartnerRepFeature)
            .execute(
                request,
                accessToken
            );

    }

    getPartnerRepW9UpdateUrl(request:GetPartnerRepW9UpdateUrlReq,
                             accessToken:string):Promise<string> {

        return this
            ._diContainer
            .get(GetPartnerRepW9UpdateUrlFeature)
            .execute(
                request,
                accessToken
            );

    }

    getPartnerRepBankInfoUpdateUrl(request:GetPartnerRepBankInfoUpdateUrlReq,
                                   accessToken:string):Promise<string> {

        return this
            ._diContainer
            .get(GetPartnerRepBankInfoUpdateUrlFeature)
            .execute(
                request,
                accessToken
            );

    }

    getPartnerRepWithId(id:number,
                        accessToken:string):Promise<PartnerRepView> {

        return this
            ._diContainer
            .get(GetPartnerRepWithIdFeature)
            .execute(
                id,
                accessToken
            );
    }

    listPartnerRepsWithAccountId(accountId:string,
                                 accessToken:string):Promise<Array> {

        return this
            ._diContainer
            .get(ListPartnerRepsWithAccountIdFeature)
            .execute(
                accountId,
                accessToken
            );
    }

    searchForPartnerRepWithEmailAddress(userId:string,
                                        accessToken:string):Promise<Array> {

        return this
            ._diContainer
            .get(SearchForPartnerRepWithEmailAddressFeature)
            .execute(
                userId,
                accessToken
            );

    }

    updatePartnerRepFirstName(request:UpdatePartnerRepFirstNameReq,
                              accessToken:string):Promise {

        return this
            ._diContainer
            .get(UpdatePartnerRepFirstNameFeature)
            .execute(
                request,
                accessToken
            );
    }

    updatePartnerRepLastName(request:UpdatePartnerRepLastNameReq,
                             accessToken:string):Promise {

        return this
            ._diContainer
            .get(UpdatePartnerRepLastNameFeature)
            .execute(request);
    }

    updatePartnerRepPhoneNumber(request:UpdatePartnerRepPhoneNumberReq,
                                accessToken:string):Promise {

        return this
            ._diContainer
            .get(UpdatePartnerRepPhoneNumberFeature)
            .execute(
                request,
                accessToken
            );
    }

    updatePartnerRepPostalAddress(request:UpdatePartnerRepPostalAddressReq,
                                  accessToken:string):Promise {

        return this
            ._diContainer
            .get(UpdatePartnerRepPostalAddressFeature)
            .execute(
                request,
                accessToken
            );
    }

}
