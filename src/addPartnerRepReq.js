export default class AddPartnerRepReq {

    _firstName:string;

    _lastName:string;

    _userId:string;

    _accountId:string;

    constructor(firstName:string,
                lastName:string,
                userId:string,
                accountId:string) {

        if (!firstName) {
            throw new TypeError('firstName required');
        }
        this._firstName = firstName;

        if (!lastName) {
            throw new TypeError('lastName required');
        }
        this._lastName = lastName;

        if (!userId) {
            throw new TypeError('userId required');
        }
        this._userId = userId;

        if (!accountId) {
            throw new TypeError('accountId required');
        }
        this._accountId = accountId;

    }

    get firstName():string {
        return this._firstName;
    }

    get lastName():string {
        return this._lastName;
    }

    get userId():string {
        return this._userId;
    }

    get accountId():string {
        return this._accountId;
    }

    toJSON() {
        return {
            firstName: this._firstName,
            lastName: this._lastName,
            userId: this._userId,
            accountId: this._accountId
        };
    }
}