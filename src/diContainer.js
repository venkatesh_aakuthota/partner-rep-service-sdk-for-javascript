import {Container} from 'aurelia-dependency-injection';
import {HttpClient} from 'aurelia-http-client';
import PartnerRepServiceSdkConfig from './partnerRepServiceSdkConfig';
import AddPartnerRepFeature from './addPartnerRepFeature';
import GetPartnerRepBankInfoUpdateUrlFeature from './getPartnerRepBankInfoUpdateUrlFeature';
import GetPartnerRepW9UpdateUrlFeature from './getPartnerRepW9UpdateUrlFeature';
import GetPartnerRepWithIdFeature from './getPartnerRepWithIdFeature';
import ListPartnerRepsWithAccountIdFeature from './listPartnerRepsWithAccountIdFeature';
import SearchForPartnerRepWithEmailAddressFeature from './searchForPartnerRepWithEmailAddressFeature';
import UpdatePartnerRepFirstNameFeature from './updatePartnerRepFirstNameFeature';
import UpdatePartnerRepLastNameFeature from './updatePartnerRepLastNameFeature';
import UpdatePartnerRepPhoneNumberFeature from './updatePartnerRepPhoneNumberFeature';
import UpdatePartnerRepPostalAddressFeature from './updatePartnerRepPostalAddressFeature';
/**
 * @class {DiContainer}
 */
export default class DiContainer {

    _container:Container;

    /**
     * @param {PartnerRepServiceSdkConfig} config
     */
    constructor(config:PartnerRepServiceSdkConfig) {

        if (!config) {
            throw 'config required';
        }

        this._container = new Container();

        this._container.registerInstance(PartnerRepServiceSdkConfig, config);
        this._container.autoRegister(HttpClient);

        this._registerFeatures();

    }

    /**
     * Resolves a single instance based on the provided key.
     * @param key The key that identifies the object to resolve.
     * @return Returns the resolved instance.
     */
    get(key:any):any {
        return this._container.get(key);
    }

    _registerFeatures() {

        this._container.autoRegister(AddPartnerRepFeature);
        this._container.autoRegister(GetPartnerRepBankInfoUpdateUrlFeature);
        this._container.autoRegister(GetPartnerRepW9UpdateUrlFeature);
        this._container.autoRegister(GetPartnerRepWithIdFeature);
        this._container.autoRegister(ListPartnerRepsWithAccountIdFeature);
        this._container.autoRegister(SearchForPartnerRepWithEmailAddressFeature);
        this._container.autoRegister(UpdatePartnerRepFirstNameFeature);
        this._container.autoRegister(UpdatePartnerRepLastNameFeature);
        this._container.autoRegister(UpdatePartnerRepPhoneNumberFeature);
        this._container.autoRegister(UpdatePartnerRepPostalAddressFeature);

    }

}
