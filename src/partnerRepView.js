import PostalAddress from 'postal-object-model';
import PartnerRepBankInfoUpdate from './partnerRepBankInfoUpdate';
import PartnerRepW9Update from './partnerRepW9Update';

/**
 * The most detailed view of a partner rep
 * @class {PartnerRepView}
 */
export default class PartnerRepView {

    _id:number;

    _firstName:string;

    _lastName:string;

    _userId:string;

    _accountId:string;

    _mostRecentlyCommittedBankInfoUpdate:PartnerRepBankInfoUpdate;

    _uncommittedBankInfoUpdate:PartnerRepBankInfoUpdate;

    _mostRecentlyCommittedW9Update:PartnerRepW9Update;

    _uncommittedW9Update:PartnerRepW9Update;

    _postalAddress:PostalAddress;

    _phoneNumber:string;

    _sapVendorNumber:string;

    /**
     *
     * @param {number} id
     * @param {lastName} firstName
     * @param {string} lastName
     * @param {string} userId
     * @param {string} accountId
     * @param {PartnerRepBankInfoUpdate|null} [mostRecentlyCommittedBankInfoUpdate]
     * @param {PartnerRepBankInfoUpdate|null} [uncommittedBankInfoUpdate]
     * @param {PartnerRepW9Update|null} [mostRecentlyCommittedW9Update]
     * @param {PartnerRepW9Update|null} [uncommittedW9Update]
     * @param {PostalAddress|null} [postalAddress]
     * @param {string|null} [phoneNumber]
     * @param {string|null} [sapVendorNumber]
     */
    constructor(id:number,
                firstName:string,
                lastName:string,
                userId:string,
                accountId:string,
                mostRecentlyCommittedBankInfoUpdate:PartnerRepBankInfoUpdate = null,
                uncommittedBankInfoUpdate:PartnerRepBankInfoUpdate = null,
                mostRecentlyCommittedW9Update:PartnerRepW9Update = null,
                uncommittedW9Update:PartnerRepW9Update = null,
                postalAddress:PostalAddress = null,
                phoneNumber:string = null,
                sapVendorNumber:string = null) {

        if (!id) {
            throw new TypeError('id required');
        }
        this._id = id;

        if (!firstName) {
            throw new TypeError('firstName required');
        }
        this._firstName = firstName;

        if (!lastName) {
            throw new TypeError('lastName required');
        }
        this._lastName = lastName;

        if (!userId) {
            throw new TypeError('userId required');
        }
        this._userId = userId;

        if (!accountId) {
            throw new TypeError('accountId required');
        }
        this._accountId = accountId;

        this._mostRecentlyCommittedBankInfoUpdate = mostRecentlyCommittedBankInfoUpdate;

        this._uncommittedBankInfoUpdate = uncommittedBankInfoUpdate;

        this._mostRecentlyCommittedW9Update = mostRecentlyCommittedW9Update;

        this._uncommittedW9Update = uncommittedW9Update;

        this._postalAddress = postalAddress;

        this._phoneNumber = phoneNumber;

        this._sapVendorNumber = sapVendorNumber;

    }

    /**
     * @returns {number}
     */
    get id():number {
        return this._id;
    }

    /**
     * @returns {string}
     */
    get firstName():string {
        return this._firstName;
    }

    /**
     * @returns {string}
     */
    get lastName():string {
        return this._lastName;
    }

    /**
     * @returns {string}
     */
    get userId():string {
        return this._userId;
    }

    /**
     * @returns {string}
     */
    get accountId():string {
        return this._accountId;
    }

    /**
     * @returns {PartnerRepBankInfoUpdate|null}
     */
    get mostRecentlyCommittedBankInfoUpdate():PartnerRepBankInfoUpdate {
        return this._mostRecentlyCommittedBankInfoUpdate;
    }

    /**
     * @returns {PartnerRepBankInfoUpdate|null}
     */
    get uncommittedBankInfoUpdate():PartnerRepBankInfoUpdate {
        return this._uncommittedBankInfoUpdate;
    }

    /**
     * @returns {PartnerRepW9Update|null}
     */
    get mostRecentlyCommittedW9Update():PartnerRepW9Update {
        return this._mostRecentlyCommittedW9Update;
    }

    /**
     * @returns {PartnerRepW9Update|null}
     */
    get uncommittedW9Update():PartnerRepW9Update {
        return this._uncommittedW9Update;
    }

    /**
     * @returns {PostalAddress|null}
     */
    get postalAddress():PostalAddress {
        return this._postalAddress;
    }

    /**
     * @returns {string|null}
     */
    get phoneNumber():string {
        return this._phoneNumber;
    }

    /**
     * @returns {string|null}
     */
    get sapVendorNumber():string {
        return this._sapVendorNumber;
    }
};