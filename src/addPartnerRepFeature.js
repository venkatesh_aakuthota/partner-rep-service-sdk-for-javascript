import {inject} from 'aurelia-dependency-injection';
import PartnerRepServiceSdkConfig from './partnerRepServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client';
import AddPartnerRepReq from './addPartnerRepReq';

@inject(PartnerRepServiceSdkConfig, HttpClient)
class AddPartnerRepFeature {

    _config:PartnerRepServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config:PartnerRepServiceSdkConfig,
                httpClient:HttpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;
    }

    /**
     * Adds a partner rep
     * @param {AddPartnerRepReq} request
     * @param {string} accessToken
     * @returns {Promise.<number>} id
     */
    execute(request:AddPartnerRepReq,
            accessToken:string):Promise<number> {

        return this._httpClient
            .createRequest('partner-reps')
            .asPost()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .withContent(request)
            .send()
            .then(response => response.content);
    }
}

export default AddPartnerRepFeature;