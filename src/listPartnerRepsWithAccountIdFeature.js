import {inject} from 'aurelia-dependency-injection';
import PartnerRepServiceSdkConfig from './partnerRepServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client';
import PartnerRepSynopsisView from './partnerRepSynopsisView';

@inject(PartnerRepServiceSdkConfig, HttpClient)
class ListPartnerRepsWithAccountIdFeature {

    _config:PartnerRepServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config,
                httpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;

    }

    /**
     * Lists the partner reps with the provided partner sap account number.
     * @param {string} accountId
     * @param {string} accessToken
     * @returns {Promise.<PartnerRepSynopsisView[]>}
     */
    execute(accountId:string,
            accessToken:string):Promise<Array> {

        return this._httpClient
            .createRequest(`partner-reps`)
            .asGet()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .withParams({
                accountId: accountId
            })
            .send()
            .then(response =>
                Array.from(
                    response.content,
                    contentItem =>
                        new PartnerRepSynopsisView(
                            contentItem.firstName,
                            contentItem.lastName,
                            contentItem.id
                        )
                )
            );
    }
}

export default ListPartnerRepsWithAccountIdFeature;