/**
 * The least detailed view of a partner rep
 * @class {PartnerRepSynopsisView}
 */
export default class PartnerRepSynopsisView {

    _firstName:string;

    _lastName:string;

    _id:number;

    constructor(firstName:string,
                lastName:string,
                id:number) {

        if (!firstName) {
            throw new TypeError('firstName required');
        }
        this._firstName = firstName;

        if (!lastName) {
            throw new TypeError('lastName required');
        }
        this._lastName = lastName;

        if (!id) {
            throw new TypeError('id required');
        }
        this._id = id;

    }

    get firstName():string {
        return this._firstName;
    }

    get lastName():string {
        return this._lastName;
    }

    get id():number {
        return this._id;
    }

}
