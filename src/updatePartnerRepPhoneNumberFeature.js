import {inject} from 'aurelia-dependency-injection';
import PartnerRepServiceSdkConfig from './partnerRepServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client';
import UpdatePartnerRepPhoneNumberReq from './updatePartnerRepPhoneNumberReq';

@inject(PartnerRepServiceSdkConfig, HttpClient)
class UpdatePartnerRepPhoneNumberFeature {

    _config:PartnerRepServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config,
                httpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;

    }

    /**
     * Gets a transient url where a partner reps bank info can be updated
     * @param {UpdatePartnerRepPhoneNumberReq} request
     * @param {string} accessToken
     * @returns {Promise}
     */
    execute(request:UpdatePartnerRepPhoneNumberReq,
            accessToken:string):Promise {

        return this._httpClient
            .createRequest(`partner-reps/${request.id}/phone-number`)
            .asPut()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .withParams({
                phoneNumber: request.phoneNumber
            })
            .send();
    }
}

export default UpdatePartnerRepPhoneNumberFeature;