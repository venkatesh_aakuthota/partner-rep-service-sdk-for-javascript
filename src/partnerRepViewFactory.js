import PartnerRepBankInfoUpdate from './partnerRepBankInfoUpdate';
import PartnerRepView from './partnerRepView';
import PartnerRepW9Update from './partnerRepW9Update';
import {PostalAddressFactory} from 'postal-object-model';

export default class PartnerRepViewFactory {

    /**
     * @param {object} data
     * @returns {PartnerRepView}
     */
    static construct(data):PartnerRepView {

        const id = data.id;

        const firstName = data.firstName;

        const lastName = data.lastName;

        const userId = data.userId;

        const accountId = data.accountId;

        let mostRecentlyCommittedBankInfoUpdate;
        if (data.mostRecentlyCommittedBankInfoUpdate) {

            mostRecentlyCommittedBankInfoUpdate =
                new PartnerRepBankInfoUpdate(
                    data.mostRecentlyCommittedBankInfoUpdate.transactionId
                );
        }

        let uncommittedBankInfoUpdate;
        if (data.uncommittedBankInfoUpdate) {

            uncommittedBankInfoUpdate =
                new PartnerRepBankInfoUpdate(
                    data.uncommittedBankInfoUpdate.transactionId
                );

        }

        let mostRecentlyCommittedW9Update;
        if (data.mostRecentlyCommittedW9Update) {

            mostRecentlyCommittedW9Update =
                new PartnerRepW9Update(
                    data.mostRecentlyCommittedW9Update.transactionId
                );

        }

        let uncommittedW9Update;
        if (data.uncommittedW9Update) {

            uncommittedW9Update =
                new PartnerRepW9Update(
                    data.uncommitedW9Update.transactionId
                );

        }

        let postalAddress;
        if (data.postalAddress) {
            postalAddress = PostalAddressFactory.construct(data.postalAddress);
        }

        const phoneNumber = data.phoneNumber;

        const sapVendorNumber = data.sapVendorNumber;

        return new PartnerRepView(
            id,
            firstName,
            lastName,
            userId,
            accountId,
            mostRecentlyCommittedBankInfoUpdate,
            uncommittedBankInfoUpdate,
            mostRecentlyCommittedW9Update,
            uncommittedW9Update,
            postalAddress,
            phoneNumber,
            sapVendorNumber
        );

    }

}
