import {inject} from 'aurelia-dependency-injection';
import PartnerRepServiceSdkConfig from './partnerRepServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client';
import UpdatePartnerRepFirstNameReq from './updatePartnerRepFirstNameReq';

@inject(PartnerRepServiceSdkConfig, HttpClient)
class UpdatePartnerRepFirstNameFeature {

    _config:PartnerRepServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config,
                httpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;

    }

    /**
     * Gets a transient url where a partner reps bank info can be updated
     * @param {UpdatePartnerRepFirstNameReq} request
     * @param {string} accessToken
     * @returns {Promise}
     */
    execute(request:UpdatePartnerRepFirstNameReq,
            accessToken:string):Promise {

        return this._httpClient
            .createRequest(`partner-reps/${request.id}/first-name`)
            .asPut()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .withParams({
                firstName: request.firstName
            })
            .send();
    }
}

export default UpdatePartnerRepFirstNameFeature;