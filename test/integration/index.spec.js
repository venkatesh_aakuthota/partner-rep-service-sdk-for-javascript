import PartnerRepServiceSdk,
{
    AddPartnerRepReq,
    GetPartnerRepBankInfoUpdateUrlReq,
    GetPartnerRepW9UpdateUrlReq
} from '../../src/index';
import factory from './factory';
import PartnerRepView from '../../src/partnerRepView';
import config from './config';
import dummy from '../dummy';

const urlRegex = /[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&\/=]*)/;

/*
 tests
 */
describe('Index module', () => {


    describe('default export', () => {
        it('should be PartnerRepServiceSdk constructor', () => {
            /*
             act
             */
            const objectUnderTest =
                new PartnerRepServiceSdk(config.partnerRepServiceSdkConfig);

            /*
             assert
             */
            expect(objectUnderTest).toEqual(jasmine.any(PartnerRepServiceSdk));

        });
    });

    describe('instance of default export', () => {

        describe('addPartnerRep method', () => {
            it('should return partnerRepId', (done) => {
                /*
                 arrange
                 */
                const objectUnderTest =
                    new PartnerRepServiceSdk(config.partnerRepServiceSdkConfig);

                let returnedPartnerRepId;

                const accountId =
                    config.accountIdOfExistingAccountWithAnSapAccountNumber;

                /*
                 act
                 */
                const actPromise =
                    objectUnderTest
                        .addPartnerRep(
                            new AddPartnerRepReq(
                                dummy.firstName,
                                dummy.lastName,
                                factory.constructUniqueEmailAddress(),
                                accountId
                            ),
                            factory.constructValidPartnerRepOAuth2AccessToken(
                                accountId
                            )
                        )
                        .then(partnerRepId => {
                            returnedPartnerRepId = partnerRepId;
                        });

                /*
                 assert
                 */

                actPromise
                    .then(() => {
                        expect(returnedPartnerRepId).toBeTruthy();
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));

            });
        });
        describe('getPartnerRepW9UpdateUrl method', () => {
            it('should return a valid url', (done) => {
                /*
                 arrange
                 */
                const objectUnderTest =
                    new PartnerRepServiceSdk(
                        config.partnerRepServiceSdkConfig
                    );

                const accountId =
                    config.accountIdOfExistingAccountWithAnSapAccountNumber;

                let idOfSeededPartnerRep;
                let returnedUrl;

                // seed partner rep
                const arrangePromise =
                    objectUnderTest
                        .addPartnerRep(
                            new AddPartnerRepReq(
                                dummy.firstName,
                                dummy.lastName,
                                factory.constructUniqueEmailAddress(),
                                accountId
                            ),
                            factory.constructValidPartnerRepOAuth2AccessToken(
                                accountId
                            )
                        )
                        .then(partnerRepId => {
                            idOfSeededPartnerRep = partnerRepId;
                        });

                /*
                 act
                 */
                const actPromise =
                    arrangePromise
                        .then(
                            () =>
                                objectUnderTest
                                    .getPartnerRepW9UpdateUrl(
                                        new GetPartnerRepW9UpdateUrlReq(
                                            idOfSeededPartnerRep,
                                            dummy.url
                                        ),
                                        factory.constructValidPartnerRepOAuth2AccessToken(
                                            accountId,
                                            idOfSeededPartnerRep
                                        )
                                    )
                        )
                        .then(partnerRepW9UpdateUrl => {
                            returnedUrl = partnerRepW9UpdateUrl;
                        });


                /*
                 assert
                 */
                actPromise
                    .then(() => {
                        expect(returnedUrl).toMatch(urlRegex);
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));

            }, 100000);
        });
        describe('getPartnerRepBankInfoUpdateUrl method', () => {
            it('should return a valid url', (done) => {
                /*
                 arrange
                 */
                const objectUnderTest =
                    new PartnerRepServiceSdk(
                        config.partnerRepServiceSdkConfig
                    );

                const accountId =
                    config.accountIdOfExistingAccountWithAnSapAccountNumber;

                let idOfSeededPartnerRep;
                let returnedUrl;

                // seed partner rep
                const arrangePromise =
                    objectUnderTest
                        .addPartnerRep(
                            new AddPartnerRepReq(
                                dummy.firstName,
                                dummy.lastName,
                                factory.constructUniqueEmailAddress(),
                                accountId
                            ),
                            factory.constructValidPartnerRepOAuth2AccessToken(
                                accountId
                            )
                        )
                        .then(partnerRepId => {
                            idOfSeededPartnerRep = partnerRepId;
                        });

                /*
                 act
                 */
                const actPromise =
                    arrangePromise
                        .then(
                            () =>
                                objectUnderTest
                                    .getPartnerRepBankInfoUpdateUrl(
                                        new GetPartnerRepBankInfoUpdateUrlReq(
                                            idOfSeededPartnerRep,
                                            dummy.url
                                        ),
                                        factory.constructValidPartnerRepOAuth2AccessToken(
                                            accountId,
                                            idOfSeededPartnerRep
                                        )
                                    )
                        )
                        .then(partnerRepBankInfoUpdateUrl => {
                            returnedUrl = partnerRepBankInfoUpdateUrl;
                        });


                /*
                 assert
                 */
                actPromise
                    .then(() => {
                        expect(returnedUrl).toMatch(urlRegex);
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));

            }, 100000);
        });
        describe('getPartnerRepWithId method', () => {
            it('should return expected PartnerRepView', (done) => {
                /*
                 arrange
                 */
                const objectUnderTest =
                    new PartnerRepServiceSdk(
                        config.partnerRepServiceSdkConfig
                    );

                const accountId =
                    config.accountIdOfExistingAccountWithAnSapAccountNumber;

                let expectedPartnerRepView;

                let actualPartnerRepView;

                // add a test partner rep
                const addPartnerRepReq =
                    new AddPartnerRepReq(
                        dummy.firstName,
                        dummy.lastName,
                        factory.constructUniqueEmailAddress(),
                        accountId
                    );

                const arrangePromise =
                    objectUnderTest
                        .addPartnerRep(
                            addPartnerRepReq,
                            factory.constructValidPartnerRepOAuth2AccessToken(
                                addPartnerRepReq.accountId
                            )
                        )
                        .then(partnerRepId => {

                            // construct expectedPartnerRepView
                            expectedPartnerRepView =
                                new PartnerRepView(
                                    partnerRepId,
                                    addPartnerRepReq.firstName,
                                    addPartnerRepReq.lastName,
                                    addPartnerRepReq.emailAddress,
                                    addPartnerRepReq.accountId
                                );

                        });

                /*
                 act
                 */
                const actPromise =
                    arrangePromise
                        .then(
                            () =>
                                objectUnderTest
                                    .getPartnerRepWithId(
                                        expectedPartnerRepView.id,
                                        factory.constructValidPartnerRepOAuth2AccessToken(
                                            addPartnerRepReq.accountId,
                                            `${expectedPartnerRepView.id}`
                                        )
                                    )
                        )
                        .then(partnerRepView => {
                            actualPartnerRepView = partnerRepView;
                        });

                /*
                 assert
                 */
                actPromise
                    .then(() => {
                        expect(actualPartnerRepView).toEqual(expectedPartnerRepView);
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));

            });
        });
        describe('listPartnerRepsWithAccountId method', () => {
            it('returns at least 1 result', done => {
                /*
                 arrange
                 */
                const objectUnderTest =
                    new PartnerRepServiceSdk(
                        config.partnerRepServiceSdkConfig
                    );

                const accountId =
                    config.accountIdOfExistingAccountWithAnSapAccountNumber;

                let actualPartnerRepSynopses;

                const arrangePromise =
                    objectUnderTest
                        .addPartnerRep(
                            new AddPartnerRepReq(
                                dummy.firstName,
                                dummy.lastName,
                                factory.constructUniqueEmailAddress(),
                                accountId
                            ),
                            factory.constructValidPartnerRepOAuth2AccessToken(
                                accountId
                            )
                        );

                /*
                 act
                 */
                const actPromise =
                    arrangePromise
                        .then(
                            () =>
                                objectUnderTest
                                    .listPartnerRepsWithAccountId(
                                        accountId,
                                        factory.constructValidPartnerRepOAuth2AccessToken(
                                            accountId
                                        )
                                    )
                        )
                        .then(partnerRepSynopses => {
                            actualPartnerRepSynopses = partnerRepSynopses;
                        });

                /*
                 assert
                 */
                actPromise
                    .then(() => {
                        expect(actualPartnerRepSynopses.length).toBeGreaterThan(0);
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));

            });
        });
        describe('searchForPartnerRepWithEmailAddress method', () => {
            it('returns null for unused email address', done => {
                /*
                 arrange
                 */
                const unknownEmailAddress =
                    factory.constructUniqueEmailAddress();

                const objectUnderTest =
                    new PartnerRepServiceSdk(
                        config.partnerRepServiceSdkConfig
                    );

                /*
                 act
                 */
                const searchResultPromise =
                    objectUnderTest
                        .searchForPartnerRepWithEmailAddress(
                            unknownEmailAddress,
                            factory.constructValidPartnerRepOAuth2AccessToken()
                        );

                /*
                 assert
                 */

                searchResultPromise
                    .then(searchResult => {
                        expect(searchResult).toBeFalsy();
                        done();
                    })
                    .catch(error=> done.fail(JSON.stringify(error)));

            });
        });
    });
});