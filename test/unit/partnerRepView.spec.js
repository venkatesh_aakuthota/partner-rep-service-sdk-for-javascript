import PartnerRepView from '../../src/partnerRepView';
import dummy from '../dummy';

/*
 tests
 */
describe('PartnerRepView class', () => {
    describe('constructor', () => {
        it('throws if id is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new PartnerRepView(
                        null,
                        dummy.firstName,
                        dummy.lastName,
                        dummy.userId,
                        dummy.accountId
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'id required');
        });
        it('sets id', () => {
            /*
             arrange
             */
            const expectedId = dummy.partnerRepId;

            /*
             act
             */
            const objectUnderTest =
                new PartnerRepView(
                    expectedId,
                    dummy.firstName,
                    dummy.lastName,
                    dummy.userId,
                    dummy.accountId
                );

            /*
             assert
             */
            const actualId = objectUnderTest.id;
            expect(actualId).toEqual(expectedId);
        });
        it('throws if firstName is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new PartnerRepView(
                        dummy.partnerRepId,
                        null,
                        dummy.lastName,
                        dummy.userId,
                        dummy.accountId
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'firstName required');
        });
        it('sets firstName', () => {
            /*
             arrange
             */
            const expectedFirstName = dummy.firstName;

            /*
             act
             */
            const objectUnderTest =
                new PartnerRepView(
                    dummy.partnerRepId,
                    expectedFirstName,
                    dummy.lastName,
                    dummy.userId,
                    dummy.accountId
                );

            /*
             assert
             */
            const actualFirstName = objectUnderTest.firstName;
            expect(actualFirstName).toEqual(expectedFirstName);
        });
        it('throws if lastName is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new PartnerRepView(
                        dummy.partnerRepId,
                        dummy.lastName,
                        null,
                        dummy.userId,
                        dummy.accountId
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'lastName required');
        });
        it('sets lastName', () => {
            /*
             arrange
             */
            const expectedLastName = dummy.lastName;

            /*
             act
             */
            const objectUnderTest =
                new PartnerRepView(
                    dummy.partnerRepId,
                    dummy.firstName,
                    expectedLastName,
                    dummy.userId,
                    dummy.accountId
                );

            /*
             assert
             */
            const actualLastName = objectUnderTest.lastName;
            expect(actualLastName).toEqual(expectedLastName);
        });
        it('throws if userId is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new PartnerRepView(
                        dummy.partnerRepId,
                        dummy.firstName,
                        dummy.lastName,
                        null,
                        dummy.accountId
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'userId required');
        });
        it('sets userId', () => {
            /*
             arrange
             */
            const expectedUserId = dummy.userId;

            /*
             act
             */
            const objectUnderTest =
                new PartnerRepView(
                    dummy.partnerRepId,
                    dummy.firstName,
                    dummy.lastName,
                    expectedUserId,
                    dummy.accountId
                );

            /*
             assert
             */
            const actualUserId = objectUnderTest.userId;
            expect(actualUserId).toEqual(expectedUserId);
        });
        it('throws if accountId is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new PartnerRepView(
                        dummy.partnerRepId,
                        dummy.firstName,
                        dummy.lastName,
                        dummy.userId,
                        null
                    );

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'accountId required');
        });
        it('sets accountId', () => {
            /*
             arrange
             */
            const expectedAccountId = dummy.accountId;

            /*
             act
             */
            const objectUnderTest =
                new PartnerRepView(
                    dummy.partnerRepId,
                    dummy.firstName,
                    dummy.lastName,
                    dummy.userId,
                    expectedAccountId
                );

            /*
             assert
             */
            const actualAccountId = objectUnderTest.accountId;
            expect(actualAccountId).toEqual(expectedAccountId);
        });
        it('does not throw if mostRecentlyCommittedBankInfoUpdate is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new PartnerRepView(
                        dummy.partnerRepId,
                        dummy.firstName,
                        dummy.lastName,
                        dummy.userId,
                        dummy.accountId,
                        null
                    );

            /*
             act/assert
             */
            expect(constructor).not.toThrow();
        });
        it('sets mostRecentlyCommittedBankInfoUpdate', () => {
            /*
             arrange
             */
            const expectedMostRecentlyCommittedBankInfoUpdate = dummy.partnerRepBankInfoUpdate;

            /*
             act
             */
            const objectUnderTest =
                new PartnerRepView(
                    dummy.partnerRepId,
                    dummy.firstName,
                    dummy.lastName,
                    dummy.userId,
                    dummy.accountId,
                    expectedMostRecentlyCommittedBankInfoUpdate
                );

            /*
             assert
             */
            const actualMostRecentlyCommittedBankInfoUpdate =
                objectUnderTest.mostRecentlyCommittedBankInfoUpdate;

            expect(actualMostRecentlyCommittedBankInfoUpdate)
                .toEqual(expectedMostRecentlyCommittedBankInfoUpdate);
        });
        it('does not throw if uncommittedBankInfoUpdate is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new PartnerRepView(
                        dummy.partnerRepId,
                        dummy.firstName,
                        dummy.lastName,
                        dummy.userId,
                        dummy.accountId,
                        dummy.partnerRepBankInfoUpdate,
                        null
                    );

            /*
             act/assert
             */
            expect(constructor).not.toThrow();
        });
        it('sets uncommittedBankInfoUpdate', () => {
            /*
             arrange
             */
            const expectedUncommittedBankInfoUpdate = dummy.partnerRepBankInfoUpdate;

            /*
             act
             */
            const objectUnderTest =
                new PartnerRepView(
                    dummy.partnerRepId,
                    dummy.firstName,
                    dummy.lastName,
                    dummy.userId,
                    dummy.accountId,
                    dummy.partnerRepBankInfoUpdate,
                    expectedUncommittedBankInfoUpdate
                );

            /*
             assert
             */
            const actualUncommittedBankInfoUpdate = objectUnderTest.uncommittedBankInfoUpdate;
            expect(actualUncommittedBankInfoUpdate).toEqual(expectedUncommittedBankInfoUpdate);
        });
        it('does not throw if mostRecentlyCommittedW9Update is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new PartnerRepView(
                        dummy.partnerRepId,
                        dummy.firstName,
                        dummy.lastName,
                        dummy.userId,
                        dummy.accountId,
                        dummy.partnerRepBankInfoUpdate,
                        dummy.partnerRepBankInfoUpdate,
                        null
                    );

            /*
             act/assert
             */
            expect(constructor).not.toThrow();
        });
        it('sets mostRecentlyCommittedW9Update', () => {
            /*
             arrange
             */
            const expectedMostRecentlyCommittedW9Update = dummy.partnerRepW9Update;

            /*
             act
             */
            const objectUnderTest =
                new PartnerRepView(
                    dummy.partnerRepId,
                    dummy.firstName,
                    dummy.lastName,
                    dummy.userId,
                    dummy.accountId,
                    dummy.partnerRepBankInfoUpdate,
                    dummy.partnerRepBankInfoUpdate,
                    expectedMostRecentlyCommittedW9Update
                );

            /*
             assert
             */
            const actualMostRecentlyCommittedW9Update = objectUnderTest.mostRecentlyCommittedW9Update;
            expect(actualMostRecentlyCommittedW9Update).toEqual(expectedMostRecentlyCommittedW9Update);
        });
        it('does not throw if uncommittedW9Update is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new PartnerRepView(
                        dummy.partnerRepId,
                        dummy.firstName,
                        dummy.lastName,
                        dummy.userId,
                        dummy.accountId,
                        dummy.partnerRepBankInfoUpdate,
                        dummy.partnerRepBankInfoUpdate,
                        dummy.partnerRepW9Update,
                        null
                    );

            /*
             act/assert
             */
            expect(constructor).not.toThrow();
        });
        it('sets uncommittedW9Update', () => {
            /*
             arrange
             */
            const expectedUncommittedW9Update = dummy.partnerRepW9Update;

            /*
             act
             */
            const objectUnderTest =
                new PartnerRepView(
                    dummy.partnerRepId,
                    dummy.firstName,
                    dummy.lastName,
                    dummy.userId,
                    dummy.accountId,
                    dummy.partnerRepBankInfoUpdate,
                    dummy.partnerRepBankInfoUpdate,
                    dummy.partnerRepW9Update,
                    expectedUncommittedW9Update
                );

            /*
             assert
             */
            const actualUncommittedW9Update = objectUnderTest.uncommittedW9Update;
            expect(actualUncommittedW9Update).toEqual(expectedUncommittedW9Update);
        });
        it('does not throw if postalAddress is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new PartnerRepView(
                        dummy.partnerRepId,
                        dummy.firstName,
                        dummy.lastName,
                        dummy.userId,
                        dummy.accountId,
                        dummy.partnerRepBankInfoUpdate,
                        dummy.partnerRepBankInfoUpdate,
                        dummy.partnerRepW9Update,
                        dummy.partnerRepW9Update,
                        null
                    );

            /*
             act/assert
             */
            expect(constructor).not.toThrow();
        });
        it('sets postalAddress', () => {
            /*
             arrange
             */
            const expectedPostalAddress = dummy.accountId;

            /*
             act
             */
            const objectUnderTest =
                new PartnerRepView(
                    dummy.partnerRepId,
                    dummy.firstName,
                    dummy.lastName,
                    dummy.userId,
                    dummy.accountId,
                    dummy.partnerRepBankInfoUpdate,
                    dummy.partnerRepBankInfoUpdate,
                    dummy.partnerRepW9Update,
                    dummy.partnerRepW9Update,
                    expectedPostalAddress
                );

            /*
             assert
             */
            const actualPostalAddress = objectUnderTest.postalAddress;
            expect(actualPostalAddress).toEqual(expectedPostalAddress);
        });
        it('does not throw if phoneNumber is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new PartnerRepView(
                        dummy.partnerRepId,
                        dummy.firstName,
                        dummy.lastName,
                        dummy.userId,
                        dummy.accountId,
                        dummy.partnerRepBankInfoUpdate,
                        dummy.partnerRepBankInfoUpdate,
                        dummy.partnerRepW9Update,
                        dummy.partnerRepW9Update,
                        dummy.postalAddress,
                        null
                    );

            /*
             act/assert
             */
            expect(constructor).not.toThrow();
        });
        it('sets phoneNumber', () => {
            /*
             arrange
             */
            const expectedPhoneNumber = dummy.accountId;

            /*
             act
             */
            const objectUnderTest =
                new PartnerRepView(
                    dummy.partnerRepId,
                    dummy.firstName,
                    dummy.lastName,
                    dummy.userId,
                    dummy.accountId,
                    dummy.partnerRepBankInfoUpdate,
                    dummy.partnerRepBankInfoUpdate,
                    dummy.partnerRepW9Update,
                    dummy.partnerRepW9Update,
                    dummy.postalAddress,
                    expectedPhoneNumber
                );

            /*
             assert
             */
            const actualPhoneNumber = objectUnderTest.phoneNumber;
            expect(actualPhoneNumber).toEqual(expectedPhoneNumber);
        });
        it('does not throw if sapVendorNumber is null', () => {
            /*
             arrange
             */
            const constructor =
                () =>
                    new PartnerRepView(
                        dummy.partnerRepId,
                        dummy.firstName,
                        dummy.lastName,
                        dummy.userId,
                        dummy.accountId,
                        dummy.partnerRepBankInfoUpdate,
                        dummy.partnerRepBankInfoUpdate,
                        dummy.partnerRepW9Update,
                        dummy.partnerRepW9Update,
                        dummy.postalAddress,
                        dummy.phoneNumber,
                        null
                    );

            /*
             act/assert
             */
            expect(constructor).not.toThrow();
        });
        it('sets sapVendorNumber', () => {
            /*
             arrange
             */
            const expectedSapVendorNumber = dummy.accountId;

            /*
             act
             */
            const objectUnderTest =
                new PartnerRepView(
                    dummy.partnerRepId,
                    dummy.firstName,
                    dummy.lastName,
                    dummy.userId,
                    dummy.accountId,
                    dummy.partnerRepBankInfoUpdate,
                    dummy.partnerRepBankInfoUpdate,
                    dummy.partnerRepW9Update,
                    dummy.partnerRepW9Update,
                    dummy.postalAddress,
                    dummy.phoneNumber,
                    expectedSapVendorNumber
                );

            /*
             assert
             */
            const actualSapVendorNumber = objectUnderTest.sapVendorNumber;
            expect(actualSapVendorNumber).toEqual(expectedSapVendorNumber);
        });

    });
});
