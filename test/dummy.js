import {PostalAddress} from 'postal-object-model';
import PartnerRepW9Update from '../src/partnerRepW9Update';
import PartnerRepBankInfoUpdate from '../src/partnerRepBankInfoUpdate';

const dummy =   {
    firstName: 'firstName',
    lastName: 'lastName',
    phoneNumber: '0000000000',
    accountId:'000000000000000000',
    sapVendorNumber:'0000000000',
    partnerRepId:1,
    userId:'email@test.com',
    url:'https://dummy-url.com',
    postalAddress: new PostalAddress(
        'street',
        'city',
        'WA',
        'postalCode',
        'US'
    ),
    transactionId:'transactionId'
};

dummy.partnerRepW9Update =
    new PartnerRepW9Update(dummy.transactionId);

dummy.partnerRepBankInfoUpdate =
    new PartnerRepBankInfoUpdate(dummy.transactionId);

/**
 * dummy objects (see: http://xunitpatterns.com/Dummy%20Object.html)
 */
export default dummy;