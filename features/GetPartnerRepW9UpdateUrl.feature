Feature: Get Partner Rep W9 Update Url
  Gets a transient url where a partner reps W9 can be updated

  Scenario: Success
    Given I provide an accessToken identifying me as the partnerRep with id partnerRepId
    And I provide a returnUrl
    When I execute getPartnerRepW9UpdateUrl
    And a w9UpdateUrl from the partner-rep-service is returned