Feature: Search For Partner Rep With Email Address
  Searches for a partner rep with the provided email address

  Scenario Outline: Success
    Given  I provide a valid accessToken
    And provide an emailAddress of a partnerRep that <emailAddress status> in the partner-rep-service
    When I execute searchForPartnerRepWithEmailAddress
    Then <result>

    Examples:
      | emailAddress status           | result                     |
      | doesn't match any partnerReps | no partnerRep is returned  |
      | matches one partnerRep        | the partnerRep is returned |